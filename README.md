## Description
  Build and deploy a simple web app to a local kubernetes cluster.

## Prerequirments
- Deployment server is ssh accessible.
  Credentials are saved to CI/CD vars:
	- $DEPLOY_HOST
	- $DEPLOY_USER
	- $DEPLOY_PORT
	- $DEPLOY_ID_RSA (var type: file)
- MicroK8s cluster is up and runnning:
  - sudo snap install microk8s --classic
  - sudo usermod -aG microk8s $USER
  - microk8s start
  - microk8s enable ingress
- To allow k8s pull images from private docker registry, gitlab-auth secred will be created.
  Credentials should be saved to CI\CD vars:
	- $REGISTRY_SERVER (registry.gitlab.com)
	- $REGISTRY_USER
	- $REGISTRY_EMAIL
	- $REGISTRY_PASSWORD

## Deploy job explanation
CI/CD runner connects to the deployment server and performs next steps:
- create gitlab-auth secret to access gitlab private docker registry of the project;
- populate deploy.yaml.template with latest image link and pass it to the deploy.yaml file on the deployment server;
- apply deploy.yaml to the k8s cluster.

## Result
If everything goes as planned, the app will be available on http://<ingress_ext_ip>:80/ping
